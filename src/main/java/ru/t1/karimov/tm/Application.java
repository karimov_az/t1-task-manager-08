package ru.t1.karimov.tm;

import ru.t1.karimov.tm.constant.ArgumentConst;
import ru.t1.karimov.tm.constant.CommandConst;
import ru.t1.karimov.tm.model.Command;
import ru.t1.karimov.tm.repository.CommandRepository;
import ru.t1.karimov.tm.util.FormatUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
            break;
            case ArgumentConst.VERSION:
                showVersion();
            break;
            case ArgumentConst.HELP:
                showHelp();
            break;
            case ArgumentConst.INFO:
                showInfo();
            break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
            break;
            case ArgumentConst.COMMANDS:
                showCommands();
            break;
            default:
                showArgumentError();
            break;
        }
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                showAbout();
            break;
            case CommandConst.VERSION:
                showVersion();
            break;
            case CommandConst.HELP:
                showHelp();
            break;
            case CommandConst.INFO:
                showInfo();
            break;
            case CommandConst.ARGUMENTS:
                showArguments();
            break;
            case CommandConst.COMMANDS:
                showCommands();
            break;
            case CommandConst.EXIT:
                exit();
            break;
            default:
                showCommandError();
            break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument not support...");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command not support...");
        System.exit(1);
    }

    private static void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usegeMemory = totalMemory - freeMemory;

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        System.out.println("Usage memory: " + FormatUtil.formatBytes(usegeMemory));
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Alfred Karimov");
        System.out.println("e-mail: alfred@karimov.ru");
        System.out.println("e-mail: alfred_test@karimov.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for(final Command command: commands) {
            if(command == null) continue;
            final String name = command.getName();
            if(name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for(final Command command: commands) {
            if(command == null) continue;
            final String argument = command.getArgument();
            if(argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for(final Command command: commands) System.out.println(command);
    }

}
